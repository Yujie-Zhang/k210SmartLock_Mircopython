# 智能门锁设备端_MircoPython

#### 介绍
智能门锁设备端.
资料说明:
源码:智能门锁设备端_MircoPython: https://gitee.com/Yujie-Zhang/k210SmartLock_Mircopython/


源码在script中.其中boot.py相当于主函数。requests.py是mircoPython一个网络库的源代码，但是我对它的库函数修改了一小部分。
Music文件是工程所用的音频文件。
使用说明:
设备有3个模式。
1.	Wifi设置模式。 
2.	人脸录入模式。
3.	正常检测模式。

1)	Wifi设置模式,
在设备开机后。前十秒钟，屏幕会有提示按下”boot”键进入wifi设置模式。在输入wifi名称和密码后，并且完成wifi连接后。屏幕会提示用户重启设备。
2)	人脸录入模式,
设备开机后。若未进入wifi设置模式。则会有十秒钟暂停。屏幕会提示按下”boot”键进入w人脸录入模式。在输入学号后,根据语音提示进行人脸录入。录入完成后用户重启设备即可。人脸录入结束后，服务端即注册了一个对应的账户。
3)	正常检测模式
不进入前两项模式则会默认进入正常检测模式。正常检测模式会实时检测是否有人脸出现。若有人脸出现则会将图片上传至服务器判决。根据服务器的指令进行开门或提示尚未取得进入权限。
若正常检测模式状态下，出现了wifi突然中断。设备会重新连接wifi。并且在LCD屏幕上显示已断开，并且提示检查wifi开启情况。


#### 软件架构
软件架构说明


#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
